<?php

namespace App\DataFixtures;

use App\Entity\Restaurant;
use App\Entity\Schedule;
use App\Entity\TimeSlot;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RestaurantFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $schedule = new Schedule(); 

        // paramétrage des horaires
        $schedule->setName('PlanningExemple'); 
        $timeSlotMorning = new TimeSlot(new DateTime(), new DateTime());
        $days = $schedule->getDays(); 
        foreach($days as $day => $timeSlot) 
        {
            $days[$day]->add($timeSlotMorning);
        }
        $schedule->setDays($days); 
        $manager->persist($schedule); 

        // creation d'un restaurant 

        $restaurant = new Restaurant(); 
        $restaurant->setName("BeerAndCo");
        $restaurant->setSchedule($schedule); 

        $manager->persist($restaurant); 


        $manager->flush();
    }
}
