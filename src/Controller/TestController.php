<?php

namespace App\Controller;

use App\Entity\Schedule;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\TimeSlot;
use App\Form\TimeSlotType;
use DateTime;
use Symfony\Component\Validator\Constraints\Time;

class TestController extends AbstractController
{
    /**
     * @Route("/test", name="test")
     */
    public function index()
    {
        $timeSlot = new TimeSlot(new DateTime(), new DateTime());
        dump($timeSlot); 

        $schedule = new Schedule(); 
        $schedule->addTimeSlot('lundi', $timeSlot); 


        // création du formulaire de test 
        $form = $this->createForm(TimeSlotType::class, $timeSlot); 
        dump($schedule); 
        return $this->render('test/index.html.twig', [
            'controller_name' => 'TestController',
            'formTimeSlot' => $form->createView(), 
        ]);
    }
}
