<?php

namespace App\Entity;


class TimeSlot
{
    
    private $openingTime;

    private $closingTime;

    public function __construct(\DateTimeInterface $openingTime, \DateTimeInterface $closingTime)
    {
        $this->openingTime = $openingTime; 
        $this->closingTime = $closingTime; 
    }


    public function getOpeningTime(): ?\DateTimeInterface
    {
        return $this->openingTime;
    }

    public function setOpeningTime(\DateTimeInterface $openingTime): self
    {
        $this->openingTime = $openingTime;

        return $this;
    }

    public function getClosingTime(): ?\DateTimeInterface
    {
        return $this->closingTime;
    }

    public function setClosingTime(\DateTimeInterface $closingTime): self
    {
        $this->closingTime = $closingTime;

        return $this;
    }
}
