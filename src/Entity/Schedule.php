<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ScheduleRepository")
 */
class Schedule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $days = [];

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Restaurant", mappedBy="schedule")
     */
    private $restaurants;

    public function __construct()
    {
        $this->restaurants = new ArrayCollection();
        $this->days = new ArrayCollection(); 
        $this->days['lundi'] = new ArrayCollection(); 
        $this->days['mardi'] = new ArrayCollection();
        $this->days['mercredi'] = new ArrayCollection(); 
        $this->days['jeudi'] = new ArrayCollection(); 
        $this->days['vendredi'] = new ArrayCollection(); 
        $this->days['samedi'] = new ArrayCollection(); 
        $this->days['dimanche'] = new ArrayCollection(); 
    }

    public function addTimeSlot($day, \App\Entity\TimeSlot $timeSlot)
    {
        $this->days[$day]->add($timeSlot);  

    }
    public function deleteTimeSlotsDay($day)
    {
        // on efface en affectant un tableau vide 
        $this->days[$day] = new ArrayCollection(); 
    }
    public function __toString()
    {
        return $this->name; 
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDays()

    {
        return $this->days;
    }

    public function setDays( $days): self
    {
        $this->days = $days;

        return $this;
    }

    /**
     * @return Collection|Restaurant[]
     */
    public function getRestaurants(): Collection
    {
        return $this->restaurants;
    }

    public function addRestaurant(Restaurant $restaurant): self
    {
        if (!$this->restaurants->contains($restaurant)) {
            $this->restaurants[] = $restaurant;
            $restaurant->setSchedule($this);
        }

        return $this;
    }

    public function removeRestaurant(Restaurant $restaurant): self
    {
        if ($this->restaurants->contains($restaurant)) {
            $this->restaurants->removeElement($restaurant);
            // set the owning side to null (unless already changed)
            if ($restaurant->getSchedule() === $this) {
                $restaurant->setSchedule(null);
            }
        }

        return $this;
    }
}
